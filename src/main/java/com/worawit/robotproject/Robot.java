/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.worawit.robotproject;

/**
 *
 * @author werapan
 */
public class Robot extends obj{

    private TableMap map;
    int fule;

    public Robot(int x, int y, char symbol, TableMap map,int fule) {
        super(symbol,x,y);
        this.map = map;
        this.fule = fule;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public char getSymbol() {
        return symbol;
    }

    public boolean walk(char direction) {
        switch (direction) {
            case 'N': 
            case 'w':
                if (walkN()) return false;
                break;
            case 'S':
            case 's':
                if (walkS()) return false;
                break;
            case 'E':
            case 'd':
                if (walkE()) return false;

                break;
            case 'W':
            case 'a':
                if (walkW()) return false;
                break;
            default:
                return false;
        }
        checkBomb();
        return true;
    }

    private void checkBomb() {
        if(map.isBomb(x, y)) {
            System.out.println("Founded Bomb!!!! (" + x + ", " + y + ")");
        }
    }
    
    private boolean canWalk(int x,int y){
        return fule>0 && map.inMap(x, y) && !map.isTree(x, y);
    }
    
    private void reduceFuel(){
        fule--;
    }
    
    private boolean walkW() {
        checkfule();
        if (canWalk(x - 1, y)) {
            x = x - 1;
        } else {
            return true;
        }
        reduceFuel();
        return false;
    }

    public void checkfule() {
        int fule = map.fillfule(x, y);
        if(fule>0){
            this.fule += fule;
        }
    }

    private boolean walkE() {
        checkfule();
        if (canWalk(x + 1, y)) {
            x = x + 1;
        } else {
            return true;
        }
        reduceFuel();
        return false;
    }

    private boolean walkS() {
        checkfule();
        if (canWalk(x, y + 1)) {
            y = y + 1;
        } else {
            return true;
        }
        reduceFuel();
        return false;
    }

    private boolean walkN() {
        checkfule();
        if (canWalk(x, y - 1)) {
            y = y - 1;
        } else {
            return true;
        }
        reduceFuel();
        return false;
    }

    public boolean isOn(int x, int y) {
        return this.x == x && this.y == y;
    }
    
    @Override
    public String toString(){
        return "Robot>> "+ super.toString() + "fule=" + fule + "}";
    }
}
